$InstallDir = "~/AppData/Roaming/Rime/"

function Install() {
  Copy-Item -Path .\dict\* -Destination $InstallDir -Recurse -Force
  Copy-Item -Path .\weasel\* -Destination $InstallDir -Recurse -Force
}

Install
