# rime-all 配置

基于 rime 输入法的配置，目前 trime 和 squirrel 用得比较多。

## 目录说明

目录结构如下：

```bash
.
├── dict
│   └── opencc
├── fonts
├── squirrel
└── trime
```

1. dict 目录为通用设置，包含小鹤双拼、明月拼音和智能英语输入方案。
2. fonts 目录包含花园明朝字体和天珩手机版字体库，推荐使用花园明朝字体。
3. squirrel, trime, fcitx5, weasel 目录分别存放 macOS, Android, fcitx5 和 windows 版的相应配置文件。

## 不同平台的配置路径

安装时首先将 dict 目录拷贝到各平台的配置目录中，然后分别拷贝平台相应的配置文件重新部署即可。

| 平台     | 部署目录                     | 其他说明 |
| -------- | ---------------------------- | -------- |
| fcitx5   | `$HOME/.local/share/fcitx5/` | Linux    |
| squirrel | `$HOME/Library/Rime`         | macOS    |
| weasel   | `%APP_DATA%\Rime`            | Windows  |
| trime    | `/sdcard/rime/`              | Android  |

### fcitx5 配置

```bash
yay -S fcitx5 fcitx5-qt fcitx5-gtk fcitx5-rime fcitx5-material-color fcitx5-configtool

echo 'export GTK_IM_MODULE=fcitx5'>>$HOME/.xprofile
echo 'export QT_IM_MODULE=fcitx5'>>$HOME/.xprofile
echo 'export XMODIFIERS=@im=fcitx5'>>$HOME/.xprofile
```

## 说明

1. 目前体验最好的是 squirrel，使用小鹤双拼时可以输入 emoji 表情以及自定义的符号短语。
2. weasel 和 fcitx5 使用相同的配置，无法支持 emoji 表情以及自定义的符号短语。
3. ~~fcitx5 还存在一个不知道如何同步词库的问题。~~
4. Open System Settings -> Input Devices -> Virtual Keyboard, then choose fcitx5.

