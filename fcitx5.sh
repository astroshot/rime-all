#!/bin/bash

dir="$HOME/.local/share/fcitx5/rime"
mkdir -p $dir
cp -rf dict/* $dir
cp -rf fcitx5/* $dir
